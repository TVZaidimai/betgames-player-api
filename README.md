# Betgames player API

### Installation
Using package.json

```json
{
  "dependency": {
    "bg-player-api": "https://gitlab.com/TVZaidimai/betgames-player-api"
  }
}
```

Alternative setup
copy index.js to your project

```html
<script src="./dist/index.js"></script>
```

### Config
| Field | Requires | Type | Default | Description |
|---|---|---|---|---|
| `target`                      | yes | `HTMLIFrameElement` | -      | Specify iframe element |
| `config.param.code`            | yes | `string`            | -      | Your partner code (ask support if you still dont have it) |
| `config.param.gameId`          | yes | `number`            | -      | Actual gameId (https://wiki.betgames.tv/index.php?title=Game-IDs) |
| `config.param.cctv`            | no  | `boolean`           | -      | Indicate CCTV stream |
| `config.xhr.url`               | yes | `string`            | -      | Endpoint which returns playback data |
| `config.xhr.headers`           | no  | `string[]`          | `{}`   | headers which will send to `config.xhr.url` with request |
| `config.playback.showControls` | no  | `boolean`           | `true` | show/hide mute button on player side |
| `config.playback.muted`        | no  | `boolean`           | `true` | Start playback muted. (https://developers.google.com/web/updates/2017/09/autoplay-policy-changes) |

### Getters
| Name | Return type | Description |
|---|---|---|
| `version`      | `string`  | Return current version of package |
| `isSameOrigin` | `boolean` | Indicate if player iframe and client are sameOrigin |

### Emitters
| Name | Param | Description                                                                                         |
|---|---|-----------------------------------------------------------------------------------------------------|
| `setup`       | `{ config, target }` | Emit data to player iframe with required configurations.                                            |
| `destroy`     | -                    | Destroy all events, clean up configuration.                                                         |
| `setQuality`  | `string`             | Allows to change playback quality, possible param: `auto/270/360/540/1080` (see `onQuality` event). |
| `setMute`     | `boolean`            | Allows to mute/unmute video, will works properly only with sameOrigin integration.                  |

### Events
| Name        | Return data                                                                                                                                                                               | Description                                                                                                        |
|-------------|-------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------|
| `onQuality` | `{ 270p: {id: "270", isActive: false}, 360p: {id: "360", isActive: false} 540p: {id: "540", isActive: false}, 1080p: {id: "1080", isActive: false}, Auto: {id: "auto", isActive: true} }` | Fired immediately when playback initialisations is done and every times when playback source has changed           |
| `onReady`   | `void`                                                                                                                                                                                    | Fired immediately when playback initialisations is done.                                                           |
| `onPause`   | `void`                                                                                                                                                                                    | Fired immediately when playback is stopped by any reason.                                                          |
| `onError`   | `Error`                                                                                                                                                                                   | Fired immediately in case if api return error `config.xhr.url`                                                                    |
| `onMute`    | `{ muted: boolean }`                                                                                                                                                                      | Fired immediately when playback initialisations is done and every times when mute state has changed by any reason. |


### Getting Started

**React**
```ts
import React from 'react';
import BGPlayerApi from 'bg-player-api';

export const BetgamesPlayer: React.FC = (props) => {
    const [isReady, setReady] = React.useState<boolean>(false);
    const iframe = React.useRef<HTMLIFrameElement>(null);
    const src = React.useRef<string>('https://player.domain');
    const config = {
        param: {
            code: props.partnerCode, // require field
            gameId: props.gameId, // require field
        },
        playback: {
            showControls: false, // optional field
            muted: false, // optional field
        },
        xhr: {
            url: 'https://feed-api.expample/get-stream/', // require field
        },
    };

    let pending = true;
    React.useEffect(() => {
        BGPlayerApi.setup({ config, target: iframe.current });

        BGPlayerApi.onReady(() => {
            if (pending) {
                setReady(true);
            }
            // do what you need when player is ready
            // example: hide loader
        });

        BGPlayerApi.onError((error: Error) => {
            if (pending) {
                setReady(false);
            }
            console.log('Error', error.message);
            // Api endpoint returned error try to reload player (recreate iframe)
        });

        BGPlayerApi.onPause(() => {
            if (pending) {
                setReady(false);
            }
            // do what you need when player is ready
            // example: show loader
        });

        BGPlayerApi.onQuality((qualities: BGQualities) => {
            // init your quality component
        });

        return () => {
            pending = false;
            setReady(false);
            BGPlayerApi.destroy();
        };
    }, [pending]);

    return (
        <iframe
            title="betgames-player"
    className="video-iframe"
    onLoad={onLoad}
    ref={iframe}
    src={src.current}
    allow="autoplay; fullscreen; encrypted-media"
        />
);
};
```
