export interface ICustomOptions {
    code: string;
    gameId: number;
    cctv?: boolean;
    source?: string;
}
export interface IRequestOptions {
    url: string;
    headers?: Record<string, string>;
}
export interface IPlaybackOptions {
    showControls?: boolean;
    muted?: boolean;
}
export interface IConfig {
    param: ICustomOptions;
    xhr: IRequestOptions;
    playback?: IPlaybackOptions;
}
export interface IOptions {
    target: HTMLIFrameElement;
    config: IConfig;
}
export interface ICallback<Params = unknown, Return = unknown> {
    (payload: Params): Return;
}
export declare type IQualities = Partial<
    Record<
        'Auto' | '1080p' | '540p' | '360p' | '270p',
        {
            id: string | number;
            isActive: boolean;
        }
    >
>;
declare class ApiFacade {
    get version(): string;
    get isSameOrigin(): boolean;
    setup(options: IOptions): void;
    destroy(): void;
    setQuality(streamId: string): void;
    setMute(muted: boolean): void;
    onQuality(callback: ICallback<IQualities>): void;
    onReady(callback: ICallback<void>): void;
    onPause(callback: ICallback<void>): void;
    onMute(callback: ICallback<{ muted: boolean }>): void;
    onError(callback: ICallback): void;
}
declare const apiFacade: ApiFacade;

export { apiFacade as default };

export {};
